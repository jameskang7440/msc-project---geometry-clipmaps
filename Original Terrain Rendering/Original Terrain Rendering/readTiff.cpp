#include "readTiff.h"

// Initialise uint32 variables which will save image width & height
uint32 imageWidth, imageHeight;

uint16 my_pow(uint16 base, int exp) {
	uint16 result = 1;
	while (exp) { 
		if (exp & 1) 
			result *= base; 
		exp >>= 1; 
		base *= base; 
	}

	return result;
}

HValue* saveData(uint16* img_data, uint16 width)
{
	// Initialise the std::vector variable
	HValue* db = (HValue*)malloc(sizeof(HValue) * width);
	uint32 i;
	for (i = 0; i < width; i++)
	{
		// Save the value into the array
		if ((double)(img_data[i]) > 65500) {	// If the pixel has wrong value
			db[i] = { 0.0, 0.0 };
		}
		else {
			db[i] = { (double)(img_data[i]), 0.0 };
		}
		
	}
	return db;
}

Database* get_data() {
	// DB which will store the z and z_c value from the image
	Database *db;
	TIFF* tif;
	tif = TIFFOpen("./tif/india.tif", "r");

	// If the tiff file is exist,
	if (tif) {
		db = (Database *)malloc(sizeof(Database));

		// tdata_t variable which will save the image grayscale data
		tdata_t buf;
		uint32 row;
		uint32 config;

		// Get the image width, height, config
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageHeight);
		TIFFGetField(tif, TIFFTAG_PLANARCONFIG, &config);

		// Assign the variable by tif size to scan each row of image
		buf = _TIFFmalloc(TIFFScanlineSize(tif));

		uint16 s, nsamples;
		uint16* data;
		// Get the number of samples in the image. Usually 1
		TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &nsamples);

		// Set the image width & height
		db->imageHeight = imageHeight;
		db->imageWidth = imageWidth;

		// Allocate 2D array by image size
		db->positions = (HValue**)malloc(sizeof(HValue*) * imageHeight);
		for (uint16 index = 0; index < imageHeight; index++) {
			db->positions[index] = (HValue*)malloc(sizeof(HValue) * imageWidth);
		}

		// It will store in each row of the grayscale data
		HValue* row_data2 = (HValue*)malloc(sizeof(HValue) * imageWidth);
		for (s = 0; s < nsamples; s++)
		{
			for (row = 0; row < imageHeight; row++)
			{
				// Read entire column data at that row.
				TIFFReadScanline(tif, buf, row, s);
				// Convert to the uint16 value
				data = static_cast<uint16*>(buf);
				// Store grayscale data
				row_data2 = saveData(data, imageWidth);
				// Store entire column grayscale data at that row
				db->positions[row] = row_data2;
				// Clear it if it already used
				row_data2 = NULL;
			}
		}
		// Free the buffer and close the tiff reader
		_TIFFfree(buf);
		TIFFClose(tif);

		return db;
	}
	else {
		return 0;
	}
}

Regions get_all_regions(double imageHeight, double cam_x, double cam_y, int clip_counts) {
	Regions result;

	// Clip region area
	ClipRegion init_clip;
	result.clips.resize(clip_counts, init_clip);

	for (uint16 i = 0; i < clip_counts; i++) {
		// Set the clip region area 
		result.clips[i].current_x_region.resize(2, 0);
		result.clips[i].current_y_region.resize(2, 0);
		result.clips[i].next_x_region.resize(2, 0);
		result.clips[i].next_y_region.resize(2, 0);

		result.clips[i].current_region = my_pow(2, clip_level_size[i]) * 10;
		result.clips[i].next_region = my_pow(2, clip_level_size[i + 1]) * 10;

		result.clips[i].current_x_region[0] = (uint16)cam_x - result.clips[i].current_region;
		result.clips[i].current_y_region[0] = (uint16)cam_y - result.clips[i].current_region;
		result.clips[i].current_x_region[1] = (uint16)cam_x + result.clips[i].current_region;
		result.clips[i].current_y_region[1] = (uint16)cam_y + result.clips[i].current_region;

		if ((i + 1) == clip_counts) {
			// Active region area
			// Set the active region area
			result.active.region = 10;
			result.active.x_region.resize(2, 0);
			result.active.y_region.resize(2, 0);

			// Find the active region area
			result.active.x_region[0] = (uint16)cam_x - result.active.region;
			result.active.y_region[0] = (uint16)cam_y - result.active.region;
			result.active.x_region[1] = (uint16)cam_x + result.active.region;
			result.active.y_region[1] = (uint16)cam_y + result.active.region;

			result.clips[i].next_x_region[0] = result.active.x_region[0];
			result.clips[i].next_y_region[0] = result.active.y_region[0];
			result.clips[i].next_x_region[1] = result.active.x_region[1];
			result.clips[i].next_y_region[1] = result.active.y_region[1];

		}
		else {
			result.clips[i].next_x_region[0] = (uint16)cam_x - result.clips[i].next_region;
			result.clips[i].next_y_region[0] = (uint16)cam_y - result.clips[i].next_region;
			result.clips[i].next_x_region[1] = (uint16)cam_x + result.clips[i].next_region;
			result.clips[i].next_y_region[1] = (uint16)cam_y + result.clips[i].next_region;
		}
	}

	return result;
}

bool region_collision_detection(Database *db, uint16 x, uint16 y) {
	
	if ((x <= 0) || (y <= 0)) {
		return true;
	}
	else if ((x >= db->imageWidth) || (y >= db->imageHeight)) {
		return true;
	}
	else {
		return false;
	}
}

void get_z_c(Database* db, Regions all_regions, double cam_x, double cam_y) {
	// The pixel point
	HValue point = {};

	// Finding the z_c value area
	uint16 x, y;

	// Update clip region z_c
	double x_diff, y_diff, z_c, min_width, w;

	for (uint16 i = 0; i < all_regions.clips.size(); i++) {
		ClipRegion current_clip = all_regions.clips[i];
		// Update clip region z_c 
		for (y = current_clip.current_y_region[0]; y <= current_clip.current_y_region[1]; y++) {
			for (x = current_clip.current_x_region[0]; x <= current_clip.current_x_region[1]; x++) {
				point = db->positions[y][x];
				point.z_c = z_c;
				db->positions[y][x] = point;
			}
		}
	}

	// Update active region z_c 
	for (y = all_regions.active.y_region[0]; y <= all_regions.active.y_region[1]; y++) {
		for (x = all_regions.active.x_region[0]; x <= all_regions.active.x_region[1]; x++) {
			point = db->positions[y][x];
			point.z_c = point.z;
			db->positions[y][x] = point;
		}
	}
}

double morphing(HValue point, uint16 x, uint16 y, double x_diff, double y_diff, double w, double cam_x, double cam_y) {
	double z_c;

	// Clip region area & morphing
	double alpha_x, alpha_y, alpha;
	alpha_y = min(max((((double)abs(y - cam_y) - y_diff) / w), 0.0), 1);
	alpha_x = min(max((((double)abs(x - cam_x) - x_diff) / w), 0.0), 1);
	alpha = max(alpha_x, alpha_y);
	z_c = (1 - alpha) * point.z + alpha * point.z_c;

	return z_c;
}

void resize_z(Database *db) {
	// Copy the current data
	uint16 x, y;
	// Resize the z and z_c value
	for (y = 0; y < imageHeight; y++) {
		for (x = 0; x < imageWidth; x++) {
			HValue point = db->positions[y][x];
			point.z = point.z / 100.0;
			db->positions[y][x] = point;
		}
	}
}

void get_normal_vector(HValue **positions, NormalVector **normalVectors, Regions all_regions) {

	double f1, f2, f3, f4;

	ClipRegion all_area = all_regions.clips[0];
	for (uint16 y = all_area.current_y_region[0]; y < all_area.current_y_region[1]; y++) {
		for (uint16 x = all_area.current_x_region[0]; x < all_area.current_x_region[1]; x++) {
			if ((x - 1) < 0) {
				f1 = 0.0;
			}
			else {
				f1 = positions[y][x - 1].z;
			}

			if ((x + 1) >= imageWidth) {
				f2 = 0.0;
			}
			else {
				f2 = positions[y][x + 1].z;
			}

			if ((y + 1) >= imageHeight) {
				f3 = 0.0;
			}
			else {
				f3 = positions[y + 1][x].z;
			}

			if ((y - 1) < 0) {
				f4 = 0.0;
			}
			else {
				f4 = positions[y - 1][x].z;
			}
			// Find and set the normal vector
			normalVectors[y][x].x = (double)(f1 - f2) / 2.0;
			normalVectors[y][x].y = (double)(f3 - f4) / 2.0;
			normalVectors[y][x].z = 1.0;
		}
	}
}