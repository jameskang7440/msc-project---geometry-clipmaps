#include "renderWindow.h"

RenderWindow::RenderWindow(QWidget* parent)
	: QWidget(parent)
{

	// Create the window layout
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

	// Create main widget
	renderWidget = new RenderWidget(this);
	windowLayout->addWidget(renderWidget);

	setLayout(windowLayout);
}

RenderWindow::~RenderWindow()
{
	delete renderWidget;
	delete windowLayout;
}

void RenderWindow::ResetInterface()
{
	// force refresh
	renderWidget->update();
}