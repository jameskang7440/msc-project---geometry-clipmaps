#ifndef VECTOR_H
#define VECTOR_H

#include <math.h>

// make a custom vector struct

template <typename Type>
struct Vector
{
	Type x, y, z;

	Vector()
	{
		x = y = z = 0;
	}

	Vector(Type x, Type y, Type z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	Vector(const Vector &v)
	{
		x = v.get_x();
		y = v.get_y();
		z = v.get_z();
	}

	const Type &get_x() const
	{
		return x;
	}

	const Type &get_y() const
	{
		return y;
	}

	const Type &get_z() const
	{
		return z;
	}

	void set_x(Type x)
	{
		this->x = x;
	}

	void set_y(Type y)
	{
		this->y = y;
	}

	void set_z(Type z)
	{
		this->z = z;
	}

	// vector addition
	Vector operator+(const Vector &v) const
	{
		return Vector(x + v.get_x(), y + v.get_y(), z + v.get_z());
	}

	// change vector direction
	Vector operator-() const
	{
		return Vector(-x, -y, -z);
	}

	// vector subtraction
	Vector operator-(const Vector &v) const
	{
		return Vector(x - v.get_x(), y - v.get_y(), z - v.get_z());
	}

	// vector scaling (multiplication)
	Vector operator*(float f) const
	{
		return Vector(x * f, y * f, z * f);
	}

	// vector scaling (division)
	Vector operator/(float f) const
	{
		return Vector(x / f, y / f, z / f);
	}

	// vector array
	Type operator[](int i) const
	{
		if (i < 0 || i > 2)
		{
			return 0;
		}
		else
		{
			const Type *p = &x;
			return *(p + i);
		}
	}

	Type dot_product(const Vector &v) const
	{
		return x * v.get_x() + y * v.get_y() + z * v.get_z();
	}

	Vector cross_product(const Vector &v) const
	{
		return Vector(y * v.get_z() - z * v.get_y(), z * v.get_x() - x * v.get_z(), x * v.get_y() - y * v.get_x());
	}

	Type get_length() const
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}

	// vector normalisation
	Vector get_normalisation() const
	{
		return Vector(x / get_length(), y / get_length(), z / get_length());
	}
};

#endif