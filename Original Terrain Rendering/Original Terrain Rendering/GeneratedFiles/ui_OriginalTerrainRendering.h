/********************************************************************************
** Form generated from reading UI file 'OriginalTerrainRendering.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ORIGINALTERRAINRENDERING_H
#define UI_ORIGINALTERRAINRENDERING_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OriginalTerrainRenderingClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *OriginalTerrainRenderingClass)
    {
        if (OriginalTerrainRenderingClass->objectName().isEmpty())
            OriginalTerrainRenderingClass->setObjectName(QString::fromUtf8("OriginalTerrainRenderingClass"));
        OriginalTerrainRenderingClass->resize(600, 400);
        menuBar = new QMenuBar(OriginalTerrainRenderingClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        OriginalTerrainRenderingClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(OriginalTerrainRenderingClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        OriginalTerrainRenderingClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(OriginalTerrainRenderingClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        OriginalTerrainRenderingClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(OriginalTerrainRenderingClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        OriginalTerrainRenderingClass->setStatusBar(statusBar);

        retranslateUi(OriginalTerrainRenderingClass);

        QMetaObject::connectSlotsByName(OriginalTerrainRenderingClass);
    } // setupUi

    void retranslateUi(QMainWindow *OriginalTerrainRenderingClass)
    {
        OriginalTerrainRenderingClass->setWindowTitle(QApplication::translate("OriginalTerrainRenderingClass", "OriginalTerrainRendering", nullptr));
    } // retranslateUi

};

namespace Ui {
    class OriginalTerrainRenderingClass: public Ui_OriginalTerrainRenderingClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ORIGINALTERRAINRENDERING_H
