#ifndef __GL_RENDER_WIDGET_H__
#define __GL_RENDER_WIDGET_H__ 1

#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <glut.h>
#include <QGLWidget>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QMouseEvent>
#include <math.h>
#include <time.h>
#include "readTiff.h"
#include "Vector.h"

class RenderWidget : public QGLWidget
{
	Q_OBJECT

	public:
		RenderWidget(QWidget *parent);
		~RenderWidget();
		double scaling = 1.0f;
		
		
	protected:
		// Called when OpenGL context is set up
		void initializeGL();
		// Called every time the widget is resized
		void resizeGL(int w, int h);
		// Called every time the widget needs painting
		void paintGL();
		// Called every time for using camera zooming
		void wheelEvent(QWheelEvent* event) override;
		// Called to move camera
		void keyPressEvent(QKeyEvent* event) override;
		// Called to use view rotate
		void mousePressEvent(QMouseEvent* mouse) override;
		void mouseReleaseEvent(QMouseEvent* mouse) override;

	private:
		// Objects area
		void geometry_clipmap();
		void resizeLookAt();			// LookAt function
		void setPerspective();			// View-frustum area
		double getRadian(double num);	// Radian convertor
		void updateFPS(clock_t start, clock_t end);	// FPS function

		double aspectRatio;
		//double camera[9] = { 1800,15,1200,1800,8,1190,0,1,0 };	// (eyeX, eyeY, eyeZ)|(centerX, centerY, cneterZ)|(upX, upY, upZ)
		double camera[9] = { 1800,30,700,1800,20,690,0,1,0 };	// (eyeX, eyeY, eyeZ)|(centerX, centerY, cneterZ)|(upX, upY, upZ)
		double scale = 1.0;
		double move_w = 0;	// Scale of Width movement
		double move_h = 0;	// Scale of Height movement

		bool pressed = false;
		bool camera_moving = true;
		bool current_moving = false;

		Vector<double> p;
		QPoint last_pos;	// Last cursor position

		double fps = 0.0;		// Frame per second
		
		FILE *fp;
};	

#endif // !__GL_RENDER_WIDGET_H__
