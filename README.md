# MSc project - geometry clipmaps

## General Description

- The content of the project is using the Geometry Clipmap to render the enormous size of terrain.

### User

- The user is able to render the terrain with using tiff image file.

## How to use this project

### From Visual Studio project
1. Install Visual Studio 2017.
2. Install the QT 5.12 32 bit version.
    1. You should install msvc2017 during QT installation.
3. Set QT on the Visual Studio.
    1. Open the Visual Studio 2017.
    2. Enter Tools > Extensions and Updates and go to Online tab.
    3. Search 'Qt Visual Studio Tools' and install it.
    4. Close and re-run the Visual Studio.
    5. Wait until finishing the 'Qt VS Tools' initialization
    7. Click 'Qt VS Tools' and click 'Qt Options'.
    8. Click 'Add' from 'Qt Versions' tab.
    9. Set the version name and set the Qt path.
        1. Find folder : Qt > 5.12.0 > msvc2017.
        2. Then click 'msvc2017' and then confirm it.
    10. Click 'OK' to finish QT setting.
4. Close Visual Studio and load 'Geometry Clipmaps.sin'.
5. Set the project setting to depend on user's condition.
    1. Go to project settings (click the spanner icon in Solution Explorer).
    2. Set the Windows SDK version. It should be depend on your Visual Studio Windows SDK version.
    3. Save the project settings and click 'Qt VS Tools'.
    4. Go to 'Qt Project Settings'.
    5. Add version on Properties tab which you set the Qt version before. The version should be 32bit version.
    6. Go to 'Qt Modules'.
    7. Click 'Core', 'GUI', 'OpenGL', and 'Widgets' and click 'OK' to save settings.
6. 'Ctrl + Shift + B' to build this project and 'Ctrl + F5' to run this project.
7. Use Arrow keys or cursor movement keys to move camera and right click with dragging to move camera focus.
### From windows EXE file
1. Before running exe file, DO NOT MODIFY OR REMOVE ALL FILES.
2. Just run one of three exe files.

## Contact

* Janghoon Kang [sc15j2k@leeds.ac.uk]
