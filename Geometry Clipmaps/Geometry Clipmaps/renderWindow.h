#ifndef __GL_RENDER_WINDOW_H__
#define __GL_RENDER_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QPushButton>
#include <QBoxLayout>
#include <cstdlib>
#include "renderWidget.h"

class RenderWindow : public QWidget
{
	Q_OBJECT

	public:
		// Constructor / Destructor
		RenderWindow(QWidget *parent);
		~RenderWindow();

		// Window Layout
		QBoxLayout *windowLayout;

		// Main widget
		RenderWidget *renderWidget;

		// Resets all the interface elements
		void ResetInterface();

	public slots:
};


#endif // !__GL_RENDER_WINDOW_H__
