#include <QApplication>
#include <QVBoxLayout>
#include <glut.h>
#include "renderWindow.h"

int main(int argc, char *argv[]) {
	glutInit(&argc, argv);

	// Create the application
	QApplication app(argc, argv);

	// Create the master widget
	RenderWindow *window = new RenderWindow(NULL);

	// Resize the window
	window->resize(1024, 512);

	// Show the window
	window->show();

	// Start to run
	app.exec();

	// Clean up
	// Delete controller
	delete window;

	// Return to caller
	return 0;
}