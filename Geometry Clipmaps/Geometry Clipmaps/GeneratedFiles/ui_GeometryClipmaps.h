/********************************************************************************
** Form generated from reading UI file 'GeometryClipmaps.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GEOMETRYCLIPMAPS_H
#define UI_GEOMETRYCLIPMAPS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GeometryClipmapsClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *GeometryClipmapsClass)
    {
        if (GeometryClipmapsClass->objectName().isEmpty())
            GeometryClipmapsClass->setObjectName(QString::fromUtf8("GeometryClipmapsClass"));
        GeometryClipmapsClass->resize(600, 400);
        menuBar = new QMenuBar(GeometryClipmapsClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        GeometryClipmapsClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(GeometryClipmapsClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        GeometryClipmapsClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(GeometryClipmapsClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        GeometryClipmapsClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(GeometryClipmapsClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        GeometryClipmapsClass->setStatusBar(statusBar);

        retranslateUi(GeometryClipmapsClass);

        QMetaObject::connectSlotsByName(GeometryClipmapsClass);
    } // setupUi

    void retranslateUi(QMainWindow *GeometryClipmapsClass)
    {
        GeometryClipmapsClass->setWindowTitle(QApplication::translate("GeometryClipmapsClass", "GeometryClipmaps", nullptr));
    } // retranslateUi

};

namespace Ui {
    class GeometryClipmapsClass: public Ui_GeometryClipmapsClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GEOMETRYCLIPMAPS_H
