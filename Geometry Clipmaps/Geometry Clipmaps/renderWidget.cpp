﻿#include "renderWidget.h"
#define PI 3.1415926535897

GLfloat matrix[16];
Database *db;
NormalVector **normVectors;
Regions	all_regions;

RenderWidget::RenderWidget(QWidget* parent)
	: QGLWidget(parent)
{
	// Camera view vector
	p.set_x(camera[3] - camera[0]);
	p.set_y(camera[4] - camera[1]);
	p.set_z(camera[5] - camera[2]);
	// Get the terrain data
	db = get_data();
	// Resize height value
	resize_z(db);
	// Get init clipmap region
	all_regions = get_all_regions(db->imageHeight, camera[0], camera[2], 5);
	// Initialise normal vector array which will store normal vector later
	normVectors = (NormalVector**)malloc(sizeof(NormalVector*)*db->imageHeight);
	for (uint16 index = 0; index < db->imageHeight; index++) {
		normVectors[index] = (NormalVector*)malloc(sizeof(NormalVector) * db->imageWidth);
	}

	fp = fopen("./FPS/fpsOutput.csv", "w");
	fprintf(fp, "%s, %s\n", "Mode","FPS value");
	setFocus();
}

RenderWidget::~RenderWidget() {
	fclose(fp);
	delete fp;
	delete db;
}

void RenderWidget::initializeGL()
{
	// Widget background colour
	glClearColor(0.529411765, 0.807843137, 0.980392157, 1.0);
}

void RenderWidget::resizeGL(int w, int h)
{
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	aspectRatio = w / h;

	setPerspective();
}

void RenderWidget::geometry_clipmap()
{
	uint16 x, y;	// x, y position
	// Get current camera location
	double current_cam_x = camera[0];
	double current_cam_z = camera[2];

	double y_step, x_step, height;	// x, y steps to use triangle strip
	NormalVector normal_vector;		// Normal vector object
	
	// Get current regions which refers the current camera location
	all_regions = get_all_regions(db->imageHeight, current_cam_x, current_cam_z, 5);
	// Copy the height data
	HValue **copy_positions = db->positions;
	// If camera is moving,
	if (camera_moving) {
		// Update the z_c value which refers the current camera location
		get_z_c(db, all_regions, current_cam_x, current_cam_z);
		// Get current height data
		HValue **copy_positions = db->positions;
		// Find the normal vector in current area
		get_normal_vector(copy_positions, normVectors, all_regions);

		current_moving = true;
		camera_moving = false;
	}
	else {
		current_moving = false;
	}


	// Clip regions area
	 /**
	 *    Clipmap shells
	 *	  T: Top, R: Right, B: Bottom, L: Left
	 *          -->
	 *    +---+---+---+---+
	 *    | T | T | T | T |
	 *    +---+---+---+---+
	 *    | L |   |   | R | |
	 *  ^ +---+---+---+---+ v
	 *  | | L |   |   | R |
	 *    +---+---+---+---+
	 *    | B | B | B | B |
	 *    +---+---+---+---+
	 *            <--
	 */

	 // Clip region area
	for (uint16 clip_level = 0; clip_level < all_regions.clips.size(); clip_level++) {
		// Get current clip region
		ClipRegion current_clip = all_regions.clips[clip_level];
		// Render the clip region
		// Top zone
		for (y = current_clip.current_y_region[0]; y < current_clip.next_y_region[0]; y++) {
			glBegin(GL_TRIANGLE_STRIP);
			for (x = current_clip.current_x_region[0]; x <= current_clip.current_x_region[1]; x++) {
				normal_vector = normVectors[y][x];
				height = copy_positions[y][x].z_c;
				if (height == 0.0) {
					glColor3d(0.0, 0.0, 1.0);
				}
				else {
					glColor3d(0.3, 0.9, 0.0);
				}
				glNormal3d(normal_vector.x, normal_vector.y, normal_vector.z);
				glVertex3d(x, height, y);

				normal_vector = normVectors[y + 1][x];
				height = copy_positions[y + 1][x].z_c;
				if (height == 0.0) {
					glColor3d(0.0, 0.0, 1.0);
				}
				else {
					glColor3d(0.3, 0.9, 0.0);
				}
				glNormal3d(normal_vector.x, normal_vector.y, normal_vector.z);
				glVertex3d(x, height, y + 1);
			}
			glEnd();
		}

		// Right zone
		for (y = current_clip.next_y_region[0]; y < current_clip.next_y_region[1]; y++) {
			glBegin(GL_TRIANGLE_STRIP);
			for (x = current_clip.next_x_region[1]; x <= current_clip.current_x_region[1]; x++) {
				normal_vector = normVectors[y][x];
				height = copy_positions[y][x].z_c;
				if (height == 0.0) {
					glColor3d(0.0, 0.0, 1.0);
				}
				else {
					glColor3d(0.3, 0.9, 0.0);
				}
				glNormal3d(normal_vector.x, normal_vector.y, normal_vector.z);
				glVertex3d(x, height, y);

				normal_vector = normVectors[y + 1][x];
				height = copy_positions[y + 1][x].z_c;
				if (height == 0.0) {
					glColor3d(0.0, 0.0, 1.0);
				}
				else {
					glColor3d(0.3, 0.9, 0.0);
				}
				glNormal3d(normal_vector.x, normal_vector.y, normal_vector.z);
				glVertex3d(x, height, y + 1);
			}
			glEnd();
		}

		// Bottom zone
		for (y = current_clip.next_y_region[1]; y < current_clip.current_y_region[1]; y++) {
			glBegin(GL_TRIANGLE_STRIP);
			for (x = current_clip.current_x_region[0]; x <= current_clip.current_x_region[1]; x++) {
				normal_vector = normVectors[y][x];
				height = copy_positions[y][x].z_c;
				if (height == 0.0) {
					glColor3d(0.0, 0.0, 1.0);
				}
				else {
					glColor3d(0.3, 0.9, 0.0);
				}
				glNormal3d(normal_vector.x, normal_vector.y, normal_vector.z);
				glVertex3d(x, height, y);

				normal_vector = normVectors[y + 1][x];
				height = copy_positions[y + 1][x].z_c;
				if (height == 0.0) {
					glColor3d(0.0, 0.0, 1.0);
				}
				else {
					glColor3d(0.3, 0.9, 0.0);
				}
				glNormal3d(normal_vector.x, normal_vector.y, normal_vector.z);
				glVertex3d(x, height, y + 1);
			}
			glEnd();
		}

		// Left zone
		for (y = current_clip.next_y_region[0]; y < current_clip.next_y_region[1]; y++) {
			glBegin(GL_TRIANGLE_STRIP);
			for (x = current_clip.current_x_region[0]; x <= current_clip.next_x_region[0]; x++) {
				normal_vector = normVectors[y][x];
				height = copy_positions[y][x].z_c;
				if (height == 0.0) {
					glColor3d(0.0, 0.0, 1.0);
				}
				else {
					glColor3d(0.3, 0.9, 0.0);
				}
				glNormal3d(normal_vector.x, normal_vector.y, normal_vector.z);
				glVertex3d(x, height, y);

				normal_vector = normVectors[y + 1][x];
				height = copy_positions[y + 1][x].z_c;
				if (height == 0.0) {
					glColor3d(0.0, 0.0, 1.0);
				}
				else {
					glColor3d(0.3, 0.9, 0.0);
				}
				glNormal3d(normal_vector.x, normal_vector.y, normal_vector.z);
				glVertex3d(x, height, y + 1);
			}
			glEnd();
		}
	}

	// Active region area
	// Render the active region
	for (y = all_regions.active.y_region[0]; y < all_regions.active.y_region[1]; y++) {
		glBegin(GL_TRIANGLE_STRIP);
		for (x = all_regions.active.x_region[0]; x <= all_regions.active.x_region[1]; x++) {
			normal_vector = normVectors[y][x];
			height = copy_positions[y][x].z_c;
			if (height == 0.0) {
				glColor3d(0.0, 0.0, 1.0);
			}
			else {
				glColor3d(0.3, 0.9, 0.0);
			}
			glNormal3d(normal_vector.x, normal_vector.y, normal_vector.z);
			glVertex3d(x, height, y);

			normal_vector = normVectors[y + 1][x];
			height = copy_positions[y + 1][x].z_c;
			if (height == 0.0) {
				glColor3d(0.0, 0.0, 1.0);
			}
			else {
				glColor3d(0.3, 0.9, 0.0);
			}
			glNormal3d(normal_vector.x, normal_vector.y, normal_vector.z);
			glVertex3d(x, height, y + 1);
		}
		glEnd();
	}
}

void RenderWidget::paintGL()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	setPerspective();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
	glShadeModel(GL_SMOOTH);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	resizeLookAt();

	GLfloat ambientColor[] = { 0.4f, 0.4f, 0.4f, 1.0f };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
	GLfloat lightColor0[] = { 0.6f, 0.6f, 0.6f, 1.0f };
	GLfloat lightPos0[] = { db->imageWidth / 2.0f, 10.0f, db->imageHeight / 2.0f, 0.0f };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightColor0);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);

	// Use clock_t to find frame per second later
	clock_t start, end;
	start = clock();	// Start to record time
	
	glPushMatrix();

	this->geometry_clipmap();	// Render the geometry clipmap

	glPopMatrix();
	
	end = clock();	// Finish to record time

	update();
	glFlush();
	
	updateFPS(start, end);
}

void RenderWidget::keyPressEvent(QKeyEvent* event)
{
	// Save current clipmap region
	Regions	prev_regions = all_regions;
	// Save current camera location and camera target vector
	double prev_camera_x = camera[0];
	double prev_camera_z = camera[2];
	double prev_target_x = camera[3];
	double prev_target_z = camera[5];

	if (event->key() == Qt::Key_Up) {
		camera[2] -= 10;
		camera[5] -= 10;
	}
	else if (event->key() == Qt::Key_Down) {
		camera[2] += 10;
		camera[5] += 10;
	}
	else if (event->key() == Qt::Key_Left) {
		camera[0] -= 10;
		camera[3] -= 10;
	}
	else if (event->key() == Qt::Key_Right) {
		camera[0] += 10;
		camera[3] += 10;
	}

	all_regions = get_all_regions(db->imageHeight, camera[0], camera[2], 5);

	// Collision detection between clipmaps and the world
	if (!region_collision_detection(db, all_regions)) {
		// Get current clipmap region which refers the current camera location
		std::cout << "Camera Pos: [" << camera[0] << ", " << camera[2] << "]" << std::endl;

		camera_moving = true;
	}
	else {
		// Rollback the camera location & target vector
		camera[0] = prev_camera_x;
		camera[2] = prev_camera_z;
		camera[3] = prev_target_x;
		camera[5] = prev_target_z;

		// Rollback the clipmap region
		all_regions = prev_regions;

		std::cout << "Cannot move more!!!" << std::endl;

		camera_moving = false;
	}
}

void RenderWidget::resizeLookAt() {
	// camera option
	// cam loc:(eyeX, eyeY, eyeZ), target loc:(centreX, centreY, centreZ), cam up-vector:(upX, upY, upZ)
	gluLookAt(camera[0], camera[1], camera[2], camera[3], camera[4], camera[5], camera[6], camera[7], camera[8]);
}

void RenderWidget::setPerspective() {
	gluPerspective(90.0, aspectRatio, 0.1, 320.0);
}

void RenderWidget::mousePressEvent(QMouseEvent* mouse) {
	if (mouse->buttons() == Qt::LeftButton) {
		pressed = true;
		// Store pressed position
		last_pos = mouse->pos();
	}
}

void RenderWidget::mouseReleaseEvent(QMouseEvent* mouse) {
	// Calculate the difference between previous pressed position and current cursor position
	int dx = mouse->pos().x() - last_pos.x();
	int dy = mouse->pos().y() - last_pos.y();
	std::cout << dx << std::endl;

	if (pressed) {
		Vector<double> transformation_matrix[3];
		Vector<double> result;
		Vector<double> camera_position(camera[0], camera[1], camera[2]);
		Vector<double> normal_vector(camera[6], camera[7], camera[8]);

		// Calculate the next moving step or angle in camera target vector
		move_h = dy * 0.01;
		move_w = getRadian(((double)dx / 10.0) / 2.0);

		// (w,v)
		double w = cos(move_w);// w
		normal_vector = normal_vector * sin(move_w);// v
		std::cout << "Normal Vector: [" << normal_vector.get_x() << ", " << normal_vector.get_y() << ", " << normal_vector.get_z() << "]" << std::endl;

		// Transformation matrix by quaternion
		transformation_matrix[0].set_x(1 - (2 * normal_vector.get_y() * normal_vector.get_y()) - (2 * normal_vector.get_z() * normal_vector.get_z()));
		transformation_matrix[0].set_y((2 * normal_vector.get_x() * normal_vector.get_y()) - (2 * w * normal_vector.get_z()));
		transformation_matrix[0].set_z((2 * normal_vector.get_x() * normal_vector.get_z()) + (2 * w * normal_vector.get_y()));

		transformation_matrix[1].set_x(2 * normal_vector.get_x() * normal_vector.get_y() + 2 * w * normal_vector.get_z());
		transformation_matrix[1].set_y(1 - 2 * (normal_vector.get_x() * normal_vector.get_x()) - 2 * (normal_vector.get_z() * normal_vector.get_z()));
		transformation_matrix[1].set_z(2 * normal_vector.get_x() * normal_vector.get_y() + 2 * w * normal_vector.get_x());

		transformation_matrix[2].set_x(2 * normal_vector.get_x() * normal_vector.get_z() - 2 * w * normal_vector.get_y());
		transformation_matrix[2].set_y(2 * normal_vector.get_y() * normal_vector.get_z() + 2 * w * normal_vector.get_x());
		transformation_matrix[2].set_z(1 - 2 * (normal_vector.get_x() * normal_vector.get_x()) - 2 * (normal_vector.get_y() * normal_vector.get_y()));

		result.set_x(transformation_matrix[0].get_x() * p.get_x() + transformation_matrix[0].get_y() * p.get_y() + transformation_matrix[0].get_z() * p.get_z());
		result.set_y(transformation_matrix[1].get_x() * p.get_x() + transformation_matrix[1].get_y() * p.get_y() + transformation_matrix[1].get_z() * p.get_z() - move_h);
		result.set_z(transformation_matrix[2].get_x() * p.get_x() + transformation_matrix[2].get_y() * p.get_y() + transformation_matrix[2].get_z() * p.get_z());

		std::cout << "Angle: " << ((double)dx / 10.0) / 2.0 << std::endl;
		std::cout << "w: " << w << std::endl;
		std::cout << "p: [" << p.get_x() << ", " << p.get_y() << ", " << p.get_z() << "]" << std::endl;
		std::cout << "prev target: [" << camera[3] << ", " << camera[4] << ", " << camera[5] << "]" << std::endl;
		std::cout << "new target: [" << camera[0] + result.get_x() << ", " << camera[1] + result.get_y() << ", " << camera[2] + result.get_z() << "]" << std::endl;

		camera[3] = camera[0] + result.get_x();
		camera[4] = camera[1] + result.get_y();
		camera[5] = camera[2] + result.get_z();

		pressed = false;
	}
}

double RenderWidget::getRadian(double num) {
	return num * (PI / 180.0);
}

void RenderWidget::updateFPS(clock_t start, clock_t end) {
	// Calculate time delta
	double time_delta = (double)(end - start) / CLOCKS_PER_SEC;
	// Calculate the frame per second
	fps = round(1.0 / time_delta);
	// Display FPS on screen
	QString fps_str = QString::number(fps);
	glColor3d(1, 1, 1);
	this->renderText(930, 20, fps_str + QString(" FPS"));

	// Record FPS to use testing
	if (current_moving) {
		fprintf(fp, "%s, ", "move");
	}
	else {
		fprintf(fp, "%s, ", "stop");
	}
	fprintf(fp, "%d\n", fps);
}