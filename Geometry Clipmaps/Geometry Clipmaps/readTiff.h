#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <inttypes.h>
#include "lib/tiffio.h"

// Height value struct
typedef struct _HValue {
	double z;		// Z value (actual height)
	double z_c;		// Z_c value (clip-level height)
} HValue;

// Normal vector struct
typedef struct _Norm {
	double x;		// Normal vector x element
	double y;		// Normal vector y element
	double z;		// Normal vector z element
} NormalVector;

// Clip Region
typedef struct _ClipRegion {
	uint16 current_region;
	uint16 next_region;
	std::vector<uint16> current_x_region;
	std::vector<uint16> current_y_region;
	std::vector<uint16> next_x_region;
	std::vector<uint16> next_y_region;
} ClipRegion;

// Active Region
typedef struct _ActiveRegion {
	uint16 region;
	std::vector<uint16> x_region;
	std::vector<uint16> y_region;
} ActiveRegion;

// Clipmap Region
typedef struct _Regions {
	std::vector<ClipRegion> clips;
	ActiveRegion active;
} Regions;

// Database struct
typedef struct _Database{
	uint16 imageWidth;		// Image width size
	uint16 imageHeight;		// Image height size
	Regions total_regions;	// All clip & active regions
	HValue **positions;		// Height values in all pixels
} Database;

// init HValue variable
const HValue init = { 0,0 };

const int clip_level_size[5] = { 5,4,3,2,1 };

// power function
uint16 my_pow(uint16 base, int exp);

// Save the grayscale value into the database
HValue* saveData(uint16* img_data, uint16 width);

// Load the height value from the tiff file
Database* get_data();

// Set the regions
Regions get_all_regions(double imageHeight, double cam_x, double cam_y, int clip_counts);

// Collision detection
bool region_collision_detection(Database *db, Regions all_regions);

// Update z_c
void get_z_c(Database* db, Regions all_regions, double cam_x, double cam_y);

// Morphing in clip boundary
double morphing(HValue point, uint16 x, uint16 y, double x_diff, double y_diff, double w, double cam_x, double cam_y);

// Resize z and z_c value
void resize_z(Database *db);

// Get the normal vector
void get_normal_vector(HValue **positions, NormalVector **normalVectors, Regions all_regions);
